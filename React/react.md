

# React入门

## 相关js库

1.react.js：React核心库

2.react-dom.js：提供操作DOM的react扩展库

3.babel.min.js：解析JSX语法代码转为JS代码库

4.prop-types.js：用于对组件标签属性进行限制



## 创建虚拟DOM的两种方式

### 使用jsx创建虚拟DOM

- 不写引号，直接写标签

```html
<script type="text/babel">
  //1. 创建虚拟dom
  const VDOM = <h1 id="title">Hello,React</h1>
  //2. 渲染虚拟dom到页面
  ReactDOM.render(VDOM,document.getElementById('test'))
</script>
```

### 使用js创建虚拟DOM

- React.createElement（标签名，标签属性，标签内容），==一般不用====，因为嵌套过于麻烦==

```html
<script type="text/javascript">
  //1. 创建虚拟dom
  const VDOM = React.createElement('h1',{id:'title'},React.createElement('span',{},'内容'))
  //2. 渲染虚拟dom到页面
  ReactDOM.render(VDOM,document.getElementById('test'))
</script>
```



## 关于虚拟DOM

1. 本质是Object类型的对象
2. 虚拟DOM比较“轻”，真实DOM比较“重”，因为虚拟DOM是React内部在用，无需真实DOM上的那么多属性
3. 虚拟DOM最终会被React转为真实DOM，呈现在页面上



## JSX语法规则

1. 定于虚拟DOM，不用写引号
2. 标签中混入JS表达式时要用{}
3. 样式的类名指定不要用class，要用className
4. 内联样式要用style={{key:value}}的形式去写
5. 只有一个根标签
6. 标签必须闭合
7. 标签首字符
   - 若小写字母开头，则将标签改为html中同名的元素，若html中没有改标签对应的同名元素，则报错
   - 若大写字母开头，react会去渲染对应的组件，若组件没有定义，则报错

```html
<script>
  const data = ['Angular', 'React', 'Vue']
  //1. 创建虚拟dom
  const VDOM = (
    <div>
      <h1>Hello,React</h1>
      <ul>
        {
          data.map((item, index) => {
            return <li key={index}>{item}</li>
          })
        }
      </ul>
    </div>
  )
  //2. 渲染虚拟dom到页面
  ReactDOM.render(VDOM, document.getElementById('test'))
</script>
```

## JS表达式与JS语句

### 表达式

一个表达式会产生一个值，可以放在任何一个需要值得地方

- a
- a+b
- demo(1)
- arr.map()
- function test(){}

### 语句(代码)

- if(){}
- for(){}
- switch(){case:xxxx}

# React面向组件编程

## 模块与组件

### 模块

1. 理解：向外提供特定功能的js程序, 一般就是一个js文件

2. 为什么要拆成模块：随着业务逻辑增加，代码越来越多且复杂。

3. 作用：复用js, 简化js的编写, 提高js运行效率

### 组件

1. 理解：用来实现局部功能效果的代码和资源的集合(html/css/js/image等等)

2. 为什么要用组件： 一个界面的功能更复杂

3. 作用：复用编码, 简化项目编码, 提高运行效率



## 模块化与组件化

### 模块化

当应用的js都以模块来编写，这个应用就是一个模块化的应用

### 组件化

当应用是以多组件的方式实现，这个应用就是一个组件化的应用

## React中定义组件

### 函数式组件

1. 声名函数，函数名首字母大写
2. 返回JSX
3. render中渲染组件<Demo/>

==注意==：函数组件中的this为undefined，因为babel编译后开启了严格模式

```html
<script>
  //1.声名函数式组件
  function Demo() {
    console.log(this) //undefined(babel编译后开启了严格模式)
    return <div>
      <h2>我是用函数定义的组件（适用于简单组件的定义）</h2>
      <span>2</span>
    </div>
  }

  //2.渲染组件
  ReactDOM.render(<Demo/>, document.getElementById('test'))
  // 执行了ReactDOM.render()
  //   1.React解析组件标签，找到了MyComponent组件
  //   2.发现组件是使用函数定义的，随后调用了函数，将返回的虚拟DOM转为真实DOM，随后呈现在页面中
</script>
```

### 类式组件

1. 继承自React.Component
2. render中的this执向组件实例对象,不再是babel解析后的undefined

```html
<script>
  //创建类式组件
  class MyComponent extends React.Component{
    render(){
      //render中的this？--MyComponent的实例对象
      console.log(this)
      //render是放在哪里的？--MyComponent的原型对象上，供实例使用
      return <h2>我是用类定义的组件（适用于复杂组件的定义）</h2>
    }
  }
  ReactDOM.render(<MyComponent/>,document.getElementById('test'))
  /*
  * 执行了ReactDOM.render(<MyComponent/>)之后，发生了什么
  * 1.React解析组件标签，找到Component组件
  * 2.发现组件是使用类定义的，随后new出来该类的实例，并通过该实例调用到原型上的render方法
  * 3.将render返回的虚拟DOM转为真实DOM，随后呈现在页面中
  * */
</script>
```

## 组件实例的三大属性

### state

1. state是组件对象最重要的属性, 值是对象(可以包含多个key-value的组合)

2. 组件被称为"状态机", 通过更新组件的state来更新对应的页面显示(重新渲染组件)

==注意==：

1. 组件中render方法中的this为组件实例对象

2. 组件自定义的方法中this为undefined，如何解决？

   -  强制绑定this: 通过函数对象的bind()

   - 赋值语句+箭头函数

3. 状态数据，不能直接修改或更新

```html
<script>
  class Weather extends React.Component{
    constructor(props) {
      super(props)
      // 初始化状态
      this.state={isHot:false}
    }
    render(){
      console.log(this)
      const {isHot} = this.state
      // 使用
      return <h1>今天天气很{isHot?'炎热':'凉爽'}</h1>
    }
  }
  ReactDOM.render(<Weather/>,document.getElementById('test'))
</script>
```

#### 获取state中的值

##### 类中方法中的this

1. 类中定义的方法会自动开启==局部严格模式==

```html
<script>
  class Student{
    constructor(name,age) {
      this.name = name
      this.age = age
    }
    say(){
      // 默认use strict
      console.log(this)
    }
  }

  let s1 = new Student('xqz',12)
  s1.say() //this指向Student实例
  let say = s1.say
  say() //this为undefined
</script>
```

##### React组件类中的this

```html
<script>
  class Weather extends React.Component{
    constructor(props) {
      super(props)
      this.state={isHot:false}
    }
    render(){
      const {isHot} = this.state
      return <h1 onClick={this.changeWeather}>今天天气很{isHot?'炎热':'凉爽'}</h1>
    }

    changeWeather() {
      // 只要通过weather的实例对象去调用了changeWeather，那么this就是实例对象
      // 由于changeWeather是作为onClick的回调， 不是通过实例调用的，是直接调用
      // 类中的方法开启了局部的严格模式，所以this为undefined
      console.log(this.state)
    }
  }
  ReactDOM.render(<Weather/>,document.getElementById('test'))

</script>
```

##### 如何解决

```html
<script>
  class Weather extends React.Component{
    constructor(props) {
      super(props)
      this.state={isHot:false}
        // this.changeWeather是原型上的方法，通过bind，将this指向实例
        // demo为实例上的方法，就是把原型上的changeWeather方法复刻一份，修改this后添加到实例身上,相当于重写
      	// 在render方法中调用的是实例身上的方法
      this.changeWeather = this.changeWeather.bind(this)
    }
    render(){
      const {isHot} = this.state
      return <h1 onClick={this.changeWeather}>今天天气很{isHot?'炎热':'凉爽'}</h1>
    }

    changeWeather() {
      console.log(this.state)
    }
  }
  ReactDOM.render(<Weather/>,document.getElementById('test'))

</script>
```

#### 修改state中的值

React.Component.prototype中有setState方法，通过组件实例.proto->组件类.prototype->React.Component父类的prototype中寻找到

1. constructor调用一次
2. render调用多次
3. changeWeather点几次掉用几次

==注意==：

1. this必须在调用了super之后才有

```html
<script>
  class Weather extends React.Component{
    constructor(props) {
      super(props)
      this.state={isHot:false}
      this.changeWeather = this.changeWeather.bind(this)
    }
    render(){
      const {isHot} = this.state
      return <h1 onClick={this.changeWeather}>今天天气很{isHot?'炎热':'凉爽'}</h1>
    }

    changeWeather() {
      const {isHot} = this.state
      //注意：状态不可直接更改，要借助内置API
      this.setState({isHot:!isHot})
      }
  }
  ReactDOM.render(<Weather/>,document.getElementById('test'))
</script>
```

#### state的简写形式

```html
<script>
  class Weather extends React.Component {
    // 此时state在每一个实例自身
    state = {isHot: false}

    render() {
      const {isHot} = this.state
      return <h1 onClick={this.changeWeather}>今天天气很{isHot ? '炎热' : '凉爽'}</h1>
    }

    // 此时方法和state一样，用赋值语句，成了实例自身的一个属性，用箭头函数，this指向Weather实例，否则普通函数this为undefined。如果不用赋值语句，那么函数是在原型上的
    changeWeather = () => {
      const {isHot} = this.state
      this.setState({isHot: !isHot})
    }
  }

  ReactDOM.render(<Weather/>, document.getElementById('test'))

</script>
```

### props

#### props的基本使用

组件实例上有props属性

==注意==：

1. props是只读的，修改会报错

```html
<script>
  class Person extends React.Component{
    render(){
        //获取值
      const {name,age,sex} = this.props
      return (
        <ul>
          <li>姓名:{name}</li>
          <li>性别:{age}</li>
          <li>年龄:{sex}</li>
        </ul>
      )
    }
  }
  //传值
  ReactDOM.render(<Person name="xqz" age="20" sex="man"/>,document.getElementById('test'))
</script>
```

#### props批量传递

使用扩展运算符传递

```html
<script>
  class Person extends React.Component{
    render(){
      const {name,age,sex} = this.props
      return (
        <ul>
          <li>姓名:{name}</li>
          <li>性别:{age}</li>
          <li>年龄:{sex}</li>
        </ul>
      )
    }
  }
  const p = {name:'老刘',age:18,sex:'女'}
  //这里的...p并不是复制对象，原生中不能用展开运算符操作对象，但是这里有react.development.js和babel所以可以，但是不能随意使用，仅仅适用于标签属性的传递，这里的{}只是代表里面要写js表达式
  ReactDOM.render(<Person {...p}/>,document.getElementById('test'))
</script>
```

#### 对props进行限制

1. 组件类的proTypes属性（react模块里的要求）：对标签属性进行类型，必要性的限制

2. PropTypes是因为引入了prop-types.js模块

   ```javascript
   // 对标签属性进行类型，必要性的限制
   Person.propTypes={
     name:PropTypes.string.isRequired, //限制name必传，且为字符串
     sex:PropTypes.string,
     age:PropTypes.number,
     speak:PropTypes.func //限制speak为function
   }
   ```

3. 组件类的defaultProps属性：指定默认标签属性

   ```javascript
   // 指定默认标签属性
   Person.defaultProps={
     sex:'不男不女',
     age:18
   }
   ```

4. 使用组件时传递

   ```javascript
   const p = {name:'老刘',age:18}
   ReactDOM.render(<Person name="xqz" age={19} sex="man" speak={speak}/>,document.getElementById('test2'))
   function speak() {
     console.log(1)
   }
   ```

#### props简写

将propTypes和defaultProps设置为组件类的静态属性

```javascript
class Person extends React.Component{
  // 对标签属性进行类型，必要性的限制
  static propTypes={
    name:PropTypes.string.isRequired, 
    sex:PropTypes.string,
    age:PropTypes.number,
    speak:PropTypes.func 
  }
  // 指定默认标签属性
  static defaultProps={
    sex:'不男不女',
    age:18
  }
  render(){
    const {name,age,sex} = this.props
    // this.props.name = 'xqz' //报错，因为props是只读的
    return (
      <ul>
        <li>姓名:{name}</li>
        <li>性别:{age+1}</li>
        <li>年龄:{sex}</li>
      </ul>
    )
  }
}
```

#### 函数式组件使用props

1. 组件传递方式不变，通过参数接收
2. 对props的限制和默认值只能写在函数外面

```javascript
function Person(props){
  console.log(props)
  const {name,age,sex} = props
  return (
    <ul>
      <li>姓名:{name}</li>
      <li>性别:{age+1}</li>
      <li>年龄:{sex}</li>
    </ul>
  )
}
  // 对标签属性进行类型，必要性的限制
  Person.propTypes={
    name:PropTypes.string.isRequired,
    sex:PropTypes.string,
    age:PropTypes.number
  }
  // 指定默认标签属性
  Person.defaultProps={
    sex:'不男不女',
    age:18
  }
ReactDOM.render(<Person name="xqz" age={19}/>,document.getElementById('test2'))
```

### refs与事件处理

#### string类型的ref（已过时）

1. 直接在标签上定义ref，通过this.refs使用
2. 会有效率问题，已经过时

```html
<script>
  class Demo extends React.Component{
    showData=()=>{
      const {input1} = this.refs
      console.log(input1.value)
    }
    showData2=()=>{
      const {input2} = this.refs
      console.log(input2.value)
    }
    render(){
      return (
        <div>
          <input ref="input1" onClick={this.showData} type="text" placeholder="点击提示"/>
          <button onClick={this.showData}>点击</button>
          <input ref="input2" onBlur={this.showData2} type="text" placeholder="失去焦点提示"/>
        </div>
      )
    }
  }
  ReactDOM.render(<Demo/>,document.getElementById('test'))
</script>
```

#### 回调函数形式的refs

##### 内联的写法（开发常用）

在更新时，ref的回调会调用两次，但是其实并没有什么影响

```html
    <script>
        class Demo extends React.Component{
            showData=()=>{
                const {input1} = this
                console.log(input1.value);
            }
            showData2=()=>{
                const {input2} = this
                console.log(input2.value);
            }
            render() {
                return(
                    <div>
                    // 这里的回调函数会传一个参数，参数为dom节点，this执向实例，在实例中添加属性input1，值为它的dom节点
                        <input ref={c => this.input1 = c}  type="text"/>
                        <button onClick={this.showData}>点击</button>
                        <input ref={c => this.input2 = c} onBlur={this.showData2} type="text"/>
                    </div>
                )
            }
        }
        ReactDOM.render(<Demo/>,document.getElementById('test'))
    </script>
```

##### class绑定的函数形式

在更新时，ref的函数不会回调

```html
    <script>
        class Demo extends React.Component{

            state = {isHot:true}
            showData=()=>{
                const {input} = this
                console.log(input.value);
            }
            changeWeather=()=>{
                const {isHot} = this.state
                this.setState({isHot:!isHot})
            }
            //回调的函数
            saveInput=(c)=>{
                  //这里其实用e.target也可以获取，然后存入state中，就是受控组件了
                this.input = c
                console.log('@',c)
            }
            render(){
                const {isHot} = this.state
                return (
                    <div>
                        <h2>今天天气{isHot?'炎热':'凉爽'}</h2>
                        // 这里更新时不会回调两次
                        <input type="text" ref={this.saveInput }/>
                        <button onClick={this.showData}>显示</button>
                        <button onClick={this.changeWeather}>改变天气</button>    
                    </div>
                )
            }
        }

        ReactDOM.render(<Demo/>,document.getElementById('test'))
    </script>
```

#### createRef的使用

1. React.createRef()创建容器，每个容器只能存放一个节点
2. 通过this.容器名.current获得dom节点

```html
    <script>
        class Demo extends React.Component{
            myRef= React.createRef()
            show=()=>{
                console.log(this.myRef.current.value);
            }
            render(){
                return(
                    <div>
                        <input ref={this.myRef} type="text"/>
                        <button onClick={this.show}>按钮</button>    
                    </div>
                )
            }
        }

        ReactDOM.render(<Demo/>,document.getElementById('test'))
    </script>
```

==注意==：

1. 不能滥用ref，如果需要获取的dom节点上绑定了事件处理函数，那么可以从函数中获取event.target

```html
    <script>
        class Demo extends React.Component{
            show=(event)=>{
                console.log(event.target.value);
            }
            render(){
                return (
                    <div>
                        <input type="text" onBlur={this.show} placeholder='失去焦点提示'/>    
                    </div>
                )
            }
        }
        
        ReactDOM.render(<Demo/>,document.getElementById('test'))
    </script>
```



## React中的构造器

1. 第一个用处，通过this.state赋值对象来初始化内部state，但是一般直接在类中使用赋值语句
2. 第二个用处，为事件绑定实例的this，但是一般用赋值语句+箭头函数解决
3. 第三个用处，需要在constructor中获取props。所以，constructor基本不用，如果用了必须写super(props)，且必须传props，不传就不能在构造器中使用this.props获取值

## React中的事件绑定

1. 驼峰命名
2. {}中写函数名，不需要调用
3. 可以将事件函数写在组件类中，通过赋值语句+箭头函数，箭头函数中的this指向实例，可以获取state中的值

```html
<script>
  class Weather extends React.Component{
    constructor(props) {
      super(props)
      this.state={isHot:false}
    }
      // 事件函数
    demo2=()=>{
        log
    }
    render(){
      console.log(this)
      const {isHot} = this.state
      
      return 
        <div>
            // 绑定事件
        <h1 onClick={demo}>今天天气很{isHot?'炎热':'凉爽'}</h1>
        	// 绑定事件
        <h1 onClick={this.demo2}>今天天气很{isHot?'炎热':'凉爽'}</h1>
        </div>
    }
  }
  ReactDOM.render(<Weather/>,document.getElementById('test'))
  // 事件函数
  function demo() {
    console.log('标题被点击了')
  }
</script>
```

## React中的事件处理

1. 可以通过参数拿到当前的dom节点

   ```html
       <script>
           class Demo extends React.Component{
               show=(event)=>{
                   console.log(event.target.value);
               }
               render(){
                   return (
                       <div>
                           <input type="text" onBlur={this.show} placeholder='失去焦点提示'/>    
                       </div>
                   )
               }
           }
           
           ReactDOM.render(<Demo/>,document.getElementById('test'))
       </script>
   ```

## 非受控组件

1. 表单中所有输入类的值是现用现取

```html
    <script>
        class Login extends React.Component{
            handleSubmit=(e)=>{
                e.preventDefault()
                const {username,password} = this
                console.log(username.value,password.value);
            }
            render() {
                return (
                    // 在点击提交时才获取到表单中的值
                    <form onSubmit={this.handleSubmit}>
                        用户名：<input ref={c=>this.username=c} type="text"/>
                        密码：<input ref={c=>this.password=c} type="text"/>
                        <button>提交</button>
                    </form>
                )
            }
        }
        ReactDOM.render(<Login/>,document.getElementById('test'))
    </script>
```

## 受控组件（推荐）

1. 输入类的表单，在输入时就将值存入state中，等哪里需要使用时直接从state中去获取，相当于vue的双向数据绑定
2. 可以避免ref的滥用

```html
    <script>
        class Login extends React.Component{
            state={
                username:'',
                password:''
            }
        // 将获取的数据保存到state中
            savePassword=(e)=>{
                this.setState({username:e.target.value})
            }
            saveUsername=(e)=>{
                this.setState({password:e.target.value})
            }
            submit=(e)=>{
                e.preventDefault()
                // 在需要使用时从state中去获取数据
                const {username,password} = this.state
                console.log(`用户名${username},密码${password}`);
            }
            render(){
                return (
                   <form onSubmit={this.submit}>
                    // 输入的数据通过当前的event.target获取
                        用户名：<input onChange={this.saveUsername} type="text"/>
                        密码：<input onChange={this.savePassword} type="text"/>
                        <button>登录</button>
                    </form>
                )
            }
        }

        ReactDOM.render(<Login/>,document.getElementById('test'))
    </script>
```

